#!/usr/bin/env python3

from sys import argv
from Bio import SeqIO

for read in SeqIO.parse(argv[1], "fasta"):
    read.letter_annotations["solexa_quality"] = [40] * len(read)
    print(read.format("fastq"), end='')
