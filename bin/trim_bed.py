#!/usr/bin/env python3

import sys
import pandas as pd
import numpy as np

# Load data:
bed_file = sys.argv[1]
outBed_file = sys.argv[2]
if len(sys.argv) == 4:
	min_cov = int(sys.argv[3])
else:
	min_cov = 200

# Get data of the bed file
data = pd.read_csv(bed_file, header=None, sep='\t')

# Minimal covereage (ex. 200). 200 is the default. 
row_before = min_cov
col_1 = list()
col_2 = list()
col_3 = list()
col_4 = list()
for row in np.array(data):
	if int(row[3]) > min_cov and int(row[3]) > int(row_before)*3/4:
		row_before = row[3]
		col_1.append(row[0])
		col_2.append(row[1])
		col_3.append(row[2])
		col_4.append(row[3])

df = pd.DataFrame({ 'col_1' : col_1, 'col_2' : col_2, 'col_3' : col_3, 'col_4' : col_4 })
df.to_csv(outBed_file, header=None, index=False, sep='\t')

