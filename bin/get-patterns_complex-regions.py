#!/usr/bin/env python3

import copy
import pandas as pd
from sys import argv 
from pysam import FastaFile

def get_repeats_k(seq, k):
    dictc_list_s_k3 = list()

    for s in range(0, k): ### start position
        # Declare variables
        dict_patterns = {} ## We will create a dictionary with all different kmers and its start and end
        # k = 3 ### kamer length
        m = copy.deepcopy(s)
        f = (len(seq)/k)-m
        # Iterate the kamer with window k and for step k for all different possible starts
        for i in range(0, round(f)):
            subs_tr = seq[s:s+k]
            if dict_patterns.get(subs_tr) == None:
                dict_patterns[subs_tr] = list()
                dict_patterns[subs_tr].append([ s , s+k ])
            else:
                if dict_patterns[subs_tr][len(dict_patterns[subs_tr])-1][1] == s:
                    dict_patterns[subs_tr][len(dict_patterns[subs_tr])-1][1] = dict_patterns[subs_tr][len(dict_patterns[subs_tr])-1][1] + k
                else:
                    dict_patterns[subs_tr].append([ s , s+k ])
            s = s + k 

        dictc_list_s_k3.append(dict_patterns)

    ### Get distance between strat and end of each patter for each different start to see which is the higher 
    ### Filter by how many times a repeat appear. We will consider the region as complex to sequence if it appears at least 4 times (filtered)
    dictc_list_d_k3 = list()
    dictc_list_d_k3_FILTERED = list()
    for l in range(0, len(dictc_list_s_k3)):
        d_dict = dict()
        d_dict_filtered = dict()
        for d in dictc_list_s_k3[l].keys():
            d_dict[d] = list()
            d_dict_filtered[d] = list()
            for pattern_rep in dictc_list_s_k3[l][d]:
                dist = pattern_rep[1] - pattern_rep[0]
                d_dict[d].append(dist)
                if dist >= k*4:
                    d_dict_filtered[d].append([pattern_rep[0], pattern_rep[1], dist])
        dictc_list_d_k3.append(d_dict)
        dictc_list_d_k3_FILTERED.append(d_dict_filtered)
        # keyvaluemax = max(zip(d_dict.values(), d_dict.keys()))

    ### Filter patters that have a repaet taht appears at least three times
    filtered_list = list()
    for i in range(0, len(dictc_list_d_k3_FILTERED)):
    # Iterate over all the items in dictionary and filter items which has even keys
        filtered_dict = dict()
        for (key, value) in dictc_list_d_k3_FILTERED[i].items():
            # Check if the list has len higher than 0 then add the list to new dictionary
            if len(value) > 0:
                filtered_dict[key] = value
        filtered_list.append(filtered_dict)

    return(filtered_list)

##### Do the same for kmers of different length. From 1 to 6
seq_file = argv[1]
sequence_fc = FastaFile(seq_file)
sequence = sequence_fc[sequence_fc.references[0]]

all_intervals_1_6 = list()
for kmer in range(1,7): ## from k=1 to k=6
    all_intervals_1_6.append(get_repeats_k(sequence, kmer)) 

### Join adjacent intervals and return intervals. 
## Get all values
final_intervals = list()
for n in range(0, len(all_intervals_1_6)):
    for step in all_intervals_1_6[n]:
        for sub_interval in range(0, len(step.keys())):
            intervals = step[list(step.keys())[sub_interval]]
            for sub_i in range(0, len(intervals)):
                final_intervals = final_intervals + list(range(intervals[sub_i][0], intervals[sub_i][1])) ## in base 0!!

## Sort and keep adjacent intervals
# final_intervals
final_intervals.sort(key=int)
unique_nums = set(final_intervals)
unique_nums = list(unique_nums)
unique_nums.sort(key=int)
unique_intervals = list()
unique_intervals.append([unique_nums[0], unique_nums[0]])

p = 0
for i in range(1, len(unique_nums)):
    if unique_nums[i] == unique_intervals[p][len(unique_intervals[p])-1] + 1:
        unique_intervals[p][len(unique_intervals[0])-1] = unique_nums[i] 
    else:
        unique_intervals.append([unique_nums[i], unique_nums[i]])
        p = p + 1

### From 0 based (python style) to starting by position 1:
result = list()
for element in range(0, len(unique_intervals)):
    result.append([unique_intervals[element][0]+1, unique_intervals[element][1]+1])

# print(result)

### Save result
result_df = pd.DataFrame(data = result, columns=['Start', 'End'])
result_df.to_csv(argv[2], index=False, header=True)

print(result_df)